Terraform-docs
=========

Role that installs Terraform-docs

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Installing Terraform-docs on targeted machine:

    - hosts: servers
      roles:
         - terraform-docs

License
-------

[MIT](LICENSE)

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
